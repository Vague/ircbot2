#!/bin/bash 
cd $(cat settings.container.dir.path) || exit 1
sudo podman ps -a | grep $(cat settings.container.name) -q && {
	echo already running
	sudo podman ps -a | grep $(cat settings.container.name) && echo yes
} || {
	echo starting
	sudo podman run --rm --name $(cat settings.container.name) \
		-dit \
		--volume ${PWD}/ircdir/:/ircdir/:Z \
		localhost/$(cat settings.container.name)
}

