myusername=$(cat /settings.irc.username)
ircdir=/ircdir/irc.server.tld/#channel

cd $ircdir || exit 1
pwd
echo my user is $myusername
tstart=$(date +%s)
echo my start is $tstart

tail -f out | while read line ; do
	echo $line 
	ts=$(echo $line | awk '{ print $1 } ')
	username=$(echo $line | awk '{ print $2 } ')
	msg=$(echo $line | cut -d ' ' -f 3- )

	if [ "$(date +%s)" -le "$tstart" ] ; then
		echo old line, skipping
		continue
	fi

	echo $username | grep $(cat /settings.irc.whitelist.usernames.operators ) -q && {
		echo listening
		echo $msg | grep -e '^!die '$myusername && {
			echo thats rude
			killall ii
			pkill sh
		}
		echo $msg | grep -e '^!hi '$myusername && {
			echo they said hi!
			echo howdy > in
		}
	}
done
