# vague
# irc bot platform

# https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-find-the-parent-process-id-with-bash/
# if it runs away 
# kill $(ps -o ppid= -p $(pgrep ii)) ; kill $(pgrep ii) ; killall start.sh ; killall tail

echo $$ > /tmp/ircbot2.pid

# no spaces
server="$(<config/server)"
nick="$(<config/nick)"
fullname="$(<config/fullname)"
ircdir="$(<config/ircdir)"

echo going with server $server nick $nick fullname $fullname ircdir $ircdir

run(){
rm -rfv irc
mkdir -pv irc
while true; do
	echo ii/ii -s $server -n $nick -f $fullname -i $ircdir 
	ii/ii -s $server -n $nick -f $fullname -i $ircdir | while read line ; do
		echo "$line"
		echo "$line" | grep "End of /MOTD command." && runOnStartup
	done >> config/ii.log
	echo restarting ii
	rm "irc/$server/startUpRan" -v
	sleep 5
done 
}

runOnStartup(){
	echo /// runOnStartup
	# guard against it running multiple times
	if [ -e "irc/$server/startUpRan" ] ; then
		continue
	else 
		touch "irc/$serverlab/startUpRan"
	fi
	for chan in config/channel* ; do	
		echo /j "$(<$chan)"
		echo /j "$(<$chan)" > irc/$server/in
	done
}

run 
