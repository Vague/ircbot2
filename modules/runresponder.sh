#!/bin/bash
# vague
# run commands

# run from inside a channel dir
echo runresponder

usernameWhitelist=$1
myUsername=$2
if [ ! -e $usernameWhitelist ] ; then
	echo provide path to username whitelist
	exit
fi

if [ ! $myUsername ] ; then
	echo nick not provided as '$2'
	myUsername=$(<../../../config/nick)
fi
if [ ! $myUsername ] ; then
	echo checked ../../../config/nick but its not there
	exit
fi

echo my username is $myUsername

# runcmd cuts off the first 3 parts
runcmd(){
	echo runcmd
	echo runcmd get \"$@\"
	echo runcmd get $@ > in
	echo run $(echo $@ | sed -e 's/\s/\n/g' | tail -n+4 ) > in
	echo $( echo $(echo $@ | sed -e 's/\s/\n/g' | tail -n+4 ) | bash 2>&1 ) > in
	echo fin > in
}

tail -f out | while read line ; do
	echo ======================================================
	echo reading $line
	username=$(echo $line | sed -e 's/\s/\n/g' | head -n 2 | tail -n 1 | tr -d '<>' )
	echo from username $username
	# ignore joins/parts
	echo $username | grep '\-\!\-' && continue

	echo $line | sed -e 's/\s/\n/g' | tail -n+3 | head -n1 | grep '!arun' && {
		echo wrun from $username
		echo looking in $usernameWhitelist
		grep ^$username$ $usernameWhitelist && {
			echo whitelist check succ
			#channel=$(pwd | sed -e 's/.*\///')
			runcmd $line
		} || {
			echo whitelist check fail
		}
		echo wrun from $username
	}

	echo $line | sed -e 's/\s/\n/g' | tail -n+3 | head -n1 | grep '!wrun' && {
		echo is it ours
		cmdarg1=$(echo $line | sed -e 's/\s/\n/g' | head -n 4 | tail -n 1 )
		echo cmdarg1 $cmdarg1
		[ ! "$myUsername" = "$cmdarg1" ] && {
			echo not for us, not our username
			continue
		}

		echo wrun from $username
		echo looking in $usernameWhitelist
		grep ^$username$ $usernameWhitelist && {
			echo whitelist check succ
			#channel=$(pwd | sed -e 's/.*\///')
			echo now running $line 
			# runcmd cuts off the first 3 parts
			runcmd a b c $(echo $line | sed -e 's/\s/\n/g' | tail -n +5)
		} || {
			echo whitelist check fail
		}
		echo wrun from $username
	}

	echo $line | sed -e 's/\s/\n/g' | tail -n+3 | head -n1 | grep '!rundisable' && {
		echo quit from $username
		echo looking in $usernameWhitelist
		grep ^$username$ $usernameWhitelist && {
			echo exit
			exit
		} || {
			echo whitelist check fail
		}
		echo rundisable from $username
	}
	

done


