#!/bin/bash
# vague
# monitors and responds to !fortune

# run from inside a channel dir
echo fortuneresponder

fortuneSrc=../../../wgetfortune.sh

tail -f out | while read line ; do
	echo ======================================================
	echo reading $line

# username process
	username=$(echo $line | sed -e 's/\s/\n/g' | head -n 2 | tail -n 1 | tr -d '<>' )
	echo from username $username

	# ignore joins/parts
	echo $line | grep '\-\!\-' && continue

	echo $line | grep "!fortune" && {
		echo -n "$username: $(bash ../../../wgetfortune.sh)" | tr '\n' ' '| sed -e 's/$/\n/'  > in
		
		
	}

done

